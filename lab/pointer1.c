#include <stdio.h>

int main(int argc, char *argv[])
{
    char ch = 'c';
    char cha[2] = {'a', 'b'};
    char *chPtr;
    char *chaPtr[] = {"aa", "bb"};
    
    chPtr = &ch;

    printf("Chars: %s", chaPtr[1]);

    return 0;
}
