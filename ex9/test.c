#include <stdio.h>

int main(int argc, char *argv[])
{
	int numbers[] = {1,2,3,4,5};
	char characters[] = {'a', 'b', 'c', 'd', 'e'};
	printf("Numbers each = %d %d %d %d\n", numbers[0],
		numbers[1], numbers[2], numbers[3]);
	printf("Characters each = %c %c %c %c\n", characters[0],
		characters[1], characters[2], characters[3]);
	printf("Characters = %s\n", characters);
	
	printf("Assign char to int and vice versa\n");
	numbers[3] = 'g';
	characters[3] = 10;
	printf("Numbers each = %d %d %d %d\n", numbers[0],
		numbers[1], numbers[2], numbers[3]);
	printf("Characters each = %c %c %c %c\n", characters[0],
		characters[1], characters[2], characters[3]);
	printf("Characters = %s", characters);

	printf("Print char out as int\n");
	printf("Characters each = %d %d %d %d\n", characters[0],
		characters[1], characters[2], characters[3]);

	printf("Assign the null byte and print out as string\n");
	printf("String before null byte = %s\n", characters);
	characters[4] = '\0';
	printf("String after null byte = %s\n", characters);

	return 0;
}
