#include <stdio.h>
//
int main(int argc, char* argv[])
{
    // initialize a char array
    char name[]={'a', 'b', 'c', 'd'};
    printf("Print out char array: %c%c%c%c\n", name[0], name[1],
            name[2], name[3]);
    name [3] = '\0';
    printf("Print as string: %s\n", name);

    return 0;

}
